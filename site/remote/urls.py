from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('remote.views',
    url(r'^change/$', 'change'),
    url(r'^change/(?P<remote_id>\d+)/$', 'change'),
    url(r'^cachemanifest/(?P<remote_id>\d+)/$', 'cachemanifest'),
    url(r'^(?P<remote_id>\d+)/(?P<page>\d+)/$', 'remote'),    
    url(r'^$', 'remote'),
)
