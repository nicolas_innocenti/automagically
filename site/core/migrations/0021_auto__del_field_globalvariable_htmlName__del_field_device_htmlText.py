# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'GlobalVariable.htmlName'
        db.delete_column('core_globalvariable', 'htmlName')

        # Deleting field 'Device.htmlText'
        db.delete_column('core_device', 'htmlText')

    def backwards(self, orm):
        # Adding field 'GlobalVariable.htmlName'
        db.add_column('core_globalvariable', 'htmlName',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=30),
                      keep_default=False)

        # Adding field 'Device.htmlText'
        db.add_column('core_device', 'htmlText',
                      self.gf('django.db.models.fields.CharField')(default='htmlname', max_length=40, unique=True),
                      keep_default=False)

    models = {
        'core.command': {
            'Meta': {'object_name': 'Command'},
            'argInt': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'cmd': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'id': ('django.db.models.fields.PositiveIntegerField', [], {'primary_key': 'True'})
        },
        'core.device': {
            'Meta': {'object_name': 'Device'},
            'activate': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'dim': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'hidden': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '40'}),
            'onOff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'default': '100'})
        },
        'core.deviceevent': {
            'Meta': {'object_name': 'DeviceEvent'},
            'command': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Command']"}),
            'device': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.RawTellstickDevice']"}),
            'doneAt': ('django.db.models.fields.DateTimeField', [], {}),
            'external': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'core.event': {
            'Meta': {'object_name': 'Event'},
            'command': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Command']"}),
            'condition': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.GlobalVariable']", 'null': 'True', 'blank': 'True'}),
            'device': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Device']"}),
            'doAt': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'schedule': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'to': "orm['core.ScheduledEvent']", 'null': 'True', 'blank': 'True'})
        },
        'core.globalvariable': {
            'Meta': {'object_name': 'GlobalVariable'},
            'dataType': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'hidden': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lastUpdated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'core.groupdevice': {
            'Meta': {'object_name': 'GroupDevice', '_ormbases': ['core.Device']},
            'device_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['core.Device']", 'unique': 'True', 'primary_key': 'True'}),
            'subDevices': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'subdevices'", 'symmetrical': 'False', 'to': "orm['core.Device']"})
        },
        'core.preset': {
            'Meta': {'object_name': 'Preset', '_ormbases': ['core.Device']},
            'device_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['core.Device']", 'unique': 'True', 'primary_key': 'True'})
        },
        'core.presetentry': {
            'Meta': {'object_name': 'PresetEntry'},
            'command': ('django.db.models.fields.related.ForeignKey', [], {'default': '102', 'to': "orm['core.Command']"}),
            'device': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Device']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'presetDevice': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'entries'", 'to': "orm['core.Preset']"})
        },
        'core.rawdeviceevent': {
            'Meta': {'object_name': 'RawDeviceEvent'},
            'controllerId': ('django.db.models.fields.IntegerField', [], {}),
            'data': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'receivedAt': ('django.db.models.fields.DateTimeField', [], {})
        },
        'core.rawtellstickdevice': {
            'Meta': {'object_name': 'RawTellstickDevice', '_ormbases': ['core.Device']},
            'code': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20', 'blank': 'True'}),
            'deviceId': ('django.db.models.fields.PositiveIntegerField', [], {'default': '9999'}),
            'device_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['core.Device']", 'unique': 'True', 'primary_key': 'True'}),
            'house': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20', 'blank': 'True'}),
            'protocol': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '40'}),
            'rawName': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'unit': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20', 'blank': 'True'})
        },
        'core.scheduledevent': {
            'Meta': {'object_name': 'ScheduledEvent'},
            'command': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Command']"}),
            'condition': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.GlobalVariable']", 'null': 'True', 'blank': 'True'}),
            'device': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Device']"}),
            'doAt': ('django.db.models.fields.TimeField', [], {'default': 'datetime.time(6, 0)'}),
            'doFriday': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'doMonday': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'doSaturday': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'doSunday': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'doThursday': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'doTuesday': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'doWednesday': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'relativeTime': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'relativeTo': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        'core.sendsignaldevice': {
            'Meta': {'object_name': 'SendSignalDevice', '_ormbases': ['core.Device']},
            'device_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['core.Device']", 'unique': 'True', 'primary_key': 'True'}),
            'signalOff': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100', 'blank': 'True'}),
            'signalOn': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'})
        },
        'core.sensoreventraw': {
            'Meta': {'object_name': 'SensorEventRaw'},
            'dataType': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '40'}),
            'protocol': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'sensor_id': ('django.db.models.fields.IntegerField', [], {}),
            'timestamp': ('django.db.models.fields.IntegerField', [], {}),
            'value': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20'})
        },
        'core.timerdevice': {
            'Meta': {'object_name': 'TimerDevice', '_ormbases': ['core.Device']},
            'command': ('django.db.models.fields.related.ForeignKey', [], {'default': '102', 'to': "orm['core.Command']"}),
            'condition': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.GlobalVariable']", 'null': 'True', 'blank': 'True'}),
            'delayMinutes': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'device_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['core.Device']", 'unique': 'True', 'primary_key': 'True'}),
            'subDevice': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'subdevice'", 'to': "orm['core.Device']"})
        },
        'core.woldevice': {
            'Meta': {'object_name': 'WolDevice', '_ormbases': ['core.Device']},
            'device_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['core.Device']", 'unique': 'True', 'primary_key': 'True'}),
            'ip': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '15', 'blank': 'True'}),
            'mac': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '17'})
        }
    }

    complete_apps = ['core']